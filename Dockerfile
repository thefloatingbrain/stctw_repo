FROM ubuntu:latest

ENV DEBIAN_FRONTEND=noninteractive
RUN date
RUN apt update
RUN apt-get update
RUN apt-get install -qq -y software-properties-common
RUN add-apt-repository ppa:ubuntu-toolchain-r/test

RUN apt update


RUN apt-get update
RUN apt-get install -y build-essential
RUN apt-get install -y gcc-11
RUN apt-get install -y g++-11
RUN update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-11 110 --slave /usr/bin/g++ g++ /usr/bin/g++-11 --slave /usr/bin/gcov gcov /usr/bin/gcov-11
RUN apt-get install -qq -y cmake
RUN apt-get install -y python3-pip
RUN apt-get install -y python3-venv
RUN apt-get install -y nano
RUN apt-get install -y git
RUN apt-get install -y libgl1-mesa-glx libgl1-mesa-dri libglfw3-dev libgles2-mesa-dev
RUN apt-get install -y pulseaudio pulseaudio-utils
RUN apt-get install -y ffmpeg
RUN apt-get install pkg-config
RUN apt-get install -y --no-install-recommends libfontenc-dev libice-dev libsm-dev libx11-xcb-dev libxaw7-dev libxcb-dri3-dev libxcb-icccm4-dev libxcb-image0-dev libxcb-keysyms1-dev libxcb-randr0-dev libxcb-render-util0-dev libxcb-render0-dev libxcb-shape0-dev libxcb-sync-dev libxcb-util-dev libxcb-xfixes0-dev libxcb-xinerama0-dev libxcb-xkb-dev libxcomposite-dev libxcursor-dev libxdamage-dev libxfixes-dev libxft-dev libxi-dev libxinerama-dev libxkbfile-dev libxmu-dev libxmuu-dev libxpm-dev libxres-dev libxss-dev libxt-dev libxtst-dev libxv-dev libxvmc-dev libxxf86vm-dev
RUN mkdir /root/workdir
WORKDIR /root/workdir
RUN python3 -m venv conan-env
RUN chmod +x ./conan-env/bin/activate
RUN sh conan-env/bin/activate
RUN pip install conan
RUN conan config init
RUN conan profile show default
RUN conan profile update settings.compiler.libcxx=libstdc++11 default
RUN conan profile update settings.compiler.version=11 default

