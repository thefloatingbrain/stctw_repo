#!/bin/python3

from container_manager import basic_manager
from pathlib import Path


IMAGE_NAME = "cppg_ultimate_dev", 
CONTAINER_NAME = "stctw_dev_env", 

def main(): 
	development_enviornment_volumes = [ 
			( Path().cwd() / "ssh", "/root/.ssh" ), 
			( Path().cwd() / "conan_env", "/root/.conan" ),  
			( Path().cwd() / "stctw_repo", "/root/workdir" )
		] 
	for volume in development_enviornment_volumes: 
		print( *volume )
	command = basic_manager.make_run_container_command( 
			IMAGE_NAME, 
			CONTAINER_NAME, 
			basic_manager.PODMAN_EXECUTOR, 
			display_options = None, 
			audio_options = None, 
			webcam_options = None, 
			use_home = True, 
			optional_volumes = development_enviornment_volumes, 
			optional_arguments = tuple( [ "--cap-add=SYS_PTRACE" ] ) 
		)
	#print( command )
	basic_manager.run_container( 
			command, 
			IMAGE_NAME, 
			CONTAINER_NAME 
		)

if __name__ == "__main__": 
	main()

