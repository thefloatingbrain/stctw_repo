#include <iostream>
#include <vector>
#include <string>
#include <utility>
#include <filesystem>
#include <random>
#include <array>
#include <concepts>
#include <cmath>
#include <SDL2/SDL.h>
#include <SDL_image.h>
#include <glm/vec3.hpp>

/*struct Atlas
{
    std::unique_ptr<SDL_Surface> atlas;
    Atlas( const char* atlas_file ) : IMG_Load( )
    Atlas( std::string atlas_file )
    Atlas( std::filesystem::path atlas_file ) : atlas_file.
}
*/

auto Round( auto from ) {
	return std::lround( from );
}

template< typename RangeParameterType = size_t >
struct NumericRange
{
	using RangeType = RangeParameterType;
	RangeType begin, end;
	constexpr NumericRange( RangeType begin, RangeType end ) noexcept : begin( begin ), end( end ) {}
	constexpr NumericRange() = default;
	constexpr NumericRange( const NumericRange& other ) = default;
	constexpr NumericRange( NumericRange&& other ) = default;
	constexpr NumericRange& operator=( const NumericRange& other ) = default;
	constexpr NumericRange& operator=( NumericRange&& other ) = default;
};


using ColorRangeType = std::array< NumericRange< Uint8 >, 4 >;

struct Vector2D {
	float x, y;
};

template< typename ParameterType >
using CleanType = std::remove_pointer_t< std::remove_reference_t< std::decay_t< ParameterType > > >;

template< typename ScalarParameterType >
concept ScalarConcept = std::is_integral< CleanType< ScalarParameterType > >::value || std::is_floating_point< CleanType< ScalarParameterType > >::value;

template< typename VectorParameterType >
concept Vector2Concept = requires( CleanType< VectorParameterType > vector ) { { vector.x } -> ScalarConcept;
	{ vector.y } -> ScalarConcept;
};

template< typename VectorParameterType >
concept Vector3Concept = requires( CleanType< VectorParameterType > vector ) {
	{ vector } -> Vector2Concept;
	{ vector.z } -> ScalarConcept;
};

template< typename VectorParameterType >
concept VectorConcept = Vector2Concept< VectorParameterType > || Vector3Concept< VectorParameterType >;

template< typename VectorParameterType, typename ComponentParameterType >
requires Vector2Concept< VectorParameterType > || Vector3Concept< VectorParameterType >
struct VectorComponentTypeFinder
{
	const ComponentParameterType& component;
	using ComponentType = CleanType< decltype( component ) >;
	VectorComponentTypeFinder( VectorParameterType vector ) : component( vector.x ) {}
};

template< VectorConcept VectorParameterType >
using ComponentType = decltype( CleanType< VectorParameterType >{ 0, 0 }.x );//decltype( VectorComponentTypeFinder{ VectorParameterType{ 0, 0 } } )::ComponentType;

template< Vector2Concept ToParameterType >
auto cast_vector( Vector2Concept auto from ) 
{
		using VectorComponentType = ComponentType< decltype( from ) >;
		return ToParameterType{ 
				static_cast< VectorComponentType >( from.x ), 
				static_cast< VectorComponentType >( from.y ) 
			};
}

template< ScalarConcept NumericParameterType >
NumericParameterType random_range( NumericParameterType begin, NumericParameterType end )
{
	static std::default_random_engine generator{};
	std::uniform_real_distribution< double > range{ 
			static_cast< double >( begin ), 
			static_cast< double >( end ) 
		};
	return static_cast< NumericParameterType >( Round( range( generator ) ) );
}

template< std::integral NumericParameterType = int >
auto random_range( NumericRange< NumericParameterType > range ) {
	return random_range( range.begin, range.end );
}

template< ScalarConcept NumericParameterType = int >
auto random_range( NumericParameterType range ) {
	return random_range( static_cast< NumericParameterType >( 0 ), range );
}

auto lerp( std::floating_point auto speed, std::integral auto delta_time ) {
	return Round( speed * delta_time );
}

SDL_Point uniform_advance( Vector2Concept auto to_advance, std::floating_point auto speed, std::integral auto elapsed_time )
{
	const auto distance = static_cast< int >( lerp( speed, elapsed_time ) );
	return SDL_Point{
			.x = to_advance.x + distance, 
			.y = to_advance.y + distance 
		};
}

auto advance( Vector2Concept auto to_advance, Vector2Concept auto velocity, std::integral auto elapsed_time )
{
	using VectorComponentType = ComponentType< decltype( to_advance ) >;
	return decltype( to_advance ) {
			.x = static_cast< VectorComponentType >( to_advance.x + lerp( velocity.x, elapsed_time ) ), 
			.y = static_cast< VectorComponentType >( to_advance.y + lerp( velocity.y, elapsed_time ) ) 
		};
}

auto random_point( Vector2Concept auto bounds )
{
	return decltype( bounds ) { 
			random_range( bounds.x ), 
			random_range( bounds.y ) 
		};
}

using TwoPointType = std::array< SDL_Point, 2 >;

TwoPointType sdl_rect_to_points( SDL_Rect rectangle )
{
	return TwoPointType{ 
			SDL_Point{ 
					rectangle.x, 
					rectangle.y
			}, 
			SDL_Point{ 
					rectangle.w, 
					rectangle.h 
				}
		};
}
template< typename PointsParameterType >
SDL_Rect points_to_sdl_rect( PointsParameterType two_points )
{
	return SDL_Rect{ 
			.x = two_points[ 0 ].x, 
			.y = two_points[ 0 ].y,  
			.w = two_points[ 1 ].x, 
			.h = two_points[ 1 ].y 
		};
}

SDL_Color choose_color( const ColorRangeType color_range )
{
	SDL_Color color = {
			.r = random_range( color_range[ 0 ] ), 
			.g = random_range( color_range[ 1 ] ), 
			.b = random_range( color_range[ 2 ] ), 
			.a= random_range( color_range[ 3 ] ) 
		};
	return color;
}

const ColorRangeType default_star_color_range = { 
			NumericRange< Uint8 >( 250, 255 ), 
			NumericRange< Uint8 >( 200, 250 ), 
			NumericRange< Uint8 >( 250, 255 ), 
			NumericRange< Uint8 >( 254, 255 )
		};

void DrawStar( 
		SDL_Renderer* renderer, 
		const Vector2D& star, 
		const ColorRangeType color_range = default_star_color_range 
	) 
{
	const auto color = choose_color( color_range );
        SDL_SetRenderDrawColor( 
			renderer, 
			color.r, 
			color.g, 
			color.b, 
			color.a
		);
	SDL_RenderDrawPoint( renderer, star.x, star.y );
}

std::vector< Vector2D > InitializeStars( const size_t number_of_stars, const SDL_Point bounds )
{
	std::vector< Vector2D > stars;
	for( size_t ii = 0; ii < number_of_stars; ++ii )
		stars.push_back( random_point( cast_vector< Vector2D >( bounds ) ) );
	return stars;
}

const size_t default_star_reset_reduction_scalar = 10;

void DrawStars( 
		SDL_Renderer* renderer, 
		std::vector< Vector2D >& stars, 
		float star_speed, 
		Uint32 elapsed_time, 
		const SDL_Point bounds, 
		size_t star_reset_reduction_scalar = default_star_reset_reduction_scalar,  
		const ColorRangeType star_color_range = default_star_color_range 
	)
{
	const size_t number_of_stars = stars.size();
	const int move_distance = lerp( star_speed, elapsed_time );
	for( Vector2D& star : stars )
	{
		star.y += move_distance;
		DrawStar( renderer, star, star_color_range );
		if( star.y >= bounds.y && random_range( number_of_stars / star_reset_reduction_scalar ) == 1 ) {
			star.y = random_range( -bounds.y / 2, bounds.y / 2 );
			star.x = random_range( bounds.x / 2 );
		}
	}
}

struct Sprite
{
    SDL_Renderer* renderer;
    SDL_Texture* atlas;
    SDL_Rect texture_coordinates;
    Sprite( SDL_Renderer* renderer, 
            SDL_Texture* atlas, 
            SDL_Rect texture_coordinates 
        ) : renderer( renderer ), 
            atlas( atlas ), 
            texture_coordinates( texture_coordinates ) {}
};

struct Object
{
    Sprite sprite;
	Vector2D coordinates;
    Object( Sprite sprite, float x, float y ) : sprite( sprite ), coordinates( Vector2D{ x, y } ) {}
    void Move( float x, float y ) {
        coordinates.x += x;
        coordinates.y += y;
    }
    void Render() {
		SDL_Rect rectangle_coordinates {
				.x = static_cast< int >( coordinates.x ), 
				.y = static_cast< int >( coordinates.y ), 
				.w = sprite.texture_coordinates.w, 
				.h = sprite.texture_coordinates.h 
			};
        SDL_RenderCopy( sprite.renderer, sprite.atlas, &sprite.texture_coordinates, &rectangle_coordinates );
    }
};

Object MakeShot( SDL_Rect shooter, Sprite shot_sprite, bool top_or_bottom )
{
    return Object {
        shot_sprite, 
        static_cast< float >( shooter.x + ( shooter.w / 2 ) ), 
        static_cast< float >( top_or_bottom == true ? shooter.y : shooter.y - shooter.h ) 
    };
}

int main()
{
    SDL_Point window_bounds = { 
	    	.x = 800, 
			.y = 600 
	};
    SDL_Init( SDL_INIT_VIDEO );
    SDL_Window* window = SDL_CreateWindow(
            "Space Invaders", 
            SDL_WINDOWPOS_CENTERED, 
            SDL_WINDOWPOS_CENTERED, 
			window_bounds.x, 
			window_bounds.y, 
            SDL_WINDOW_SHOWN
        );
    SDL_Renderer* renderer = SDL_CreateRenderer( window, -1, 0 );
    bool quit = false;
    SDL_Event e;
    SDL_Surface* test_surface = IMG_Load( "assets/BugSprites.png" );
    auto surface_color_key = SDL_MapRGB( test_surface->format, 0, 0, 0 );
    SDL_SetColorKey( test_surface, SDL_TRUE, surface_color_key );
    SDL_Texture* test_texture = SDL_CreateTextureFromSurface( renderer, test_surface );
    auto stars = InitializeStars( 1000, window_bounds );
    SDL_Rect ship_rect = {
        .x = 58, 
        .y = 168, 
        .w = 86 - 58, 
        .h = 189 - 168
    };
    SDL_Rect playa = {
        .x = 400,
        .y = 500,
        .w = 64, 
        .h = 64 
    };
    SDL_Rect playa_bullet = {
        .x = 345, 
        .y = 209, 
        .w = 14, 
        .h = 20 
    };
    Sprite bullet_sprite( renderer, test_texture, playa_bullet );
    std::vector< Object > playa_shots;
    float playa_speed = .75f;
    int color = 0;
    int last_color = 0;
    float bullet_speed = 2.f;
    Uint64 elapsed_time = 100;
    Uint64 start_ticks = SDL_GetTicks64();
    while( !quit )
    {
        start_ticks = SDL_GetTicks64();
        SDL_SetRenderDrawColor( renderer, 0, 0, 0, 0 );
        SDL_RenderClear( renderer );
		DrawStars( renderer, stars, .01f, elapsed_time, window_bounds );
        SDL_RenderCopy( renderer, test_texture, &ship_rect, &playa );
        SDL_RenderPresent( renderer );
        
        while( SDL_PollEvent( &e ) != 0 )
        {
            switch (e.type)
            {
                case SDL_QUIT : {
                    quit = true;
                    break;
                }
                case SDL_KEYDOWN
                : { 
                    switch (e.key.keysym.sym)
                    {
                        case SDLK_SPACE 
                        : {
                            if( e.key.repeat == 0 )
                                playa_shots.push_back( MakeShot( playa, bullet_sprite, true ) );
                            break;
                        }
                        default : break;
                    }
                    break;
                }
                default : break;
            }
        }
        auto* keyboard_state = SDL_GetKeyboardState( NULL );
        if( keyboard_state[ SDL_SCANCODE_LEFT ] )
            playa.x -= playa_speed;
        if( keyboard_state[ SDL_SCANCODE_RIGHT ] )
            playa.x += playa_speed;
        elapsed_time = SDL_GetTicks64() - start_ticks;
    }
    return 0;
}

