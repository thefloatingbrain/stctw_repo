#include <iostream>
#include <vector>
#include <string>
#include <utility>
#include <filesystem>
#include <random>
#include <array>
#include <concepts>
#include <cmath>
#include <SDL2/SDL.h>
#include <SDL_image.h>
#include <glm/vec3.hpp>

/*struct Atlas
{
    std::unique_ptr<SDL_Surface> atlas;
    Atlas( const char* atlas_file ) : IMG_Load( )
    Atlas( std::string atlas_file )
    Atlas( std::filesystem::path atlas_file ) : atlas_file.
}
*/

auto Round( auto from ) {
	return std::lround( from );
}

template< typename RangeParameterType = size_t >
struct NumericRange
{
	using RangeType = RangeParameterType;
	RangeType begin, end;
	constexpr NumericRange( RangeType begin, RangeType end ) noexcept : begin( begin ), end( end ) {}
	constexpr NumericRange() = default;
	constexpr NumericRange( const NumericRange& other ) = default;
	constexpr NumericRange( NumericRange&& other ) = default;
	constexpr NumericRange& operator=( const NumericRange& other ) = default;
	constexpr NumericRange& operator=( NumericRange&& other ) = default;
};

// Coordinates in game space will be scaled up from screen coordinates by this amount //
constinit const static auto default_game_scale = 1000;

using ColorRangeType = std::array< NumericRange< Uint8 >, 4 >;

struct Sprite
{
    SDL_Renderer* renderer;
    SDL_Surface* atlas;
    SDL_Texture* texture;
    SDL_Rect texture_coordinates;
    int width, height;
    Sprite( SDL_Renderer* renderer, 
            SDL_Surface* atlas, 
            SDL_Rect texture_coordinates, 
            int width, 
            int height 
        ) : renderer( renderer ), 
            texture_coordinates( texture_coordinates ), 
            width( width ), 
            height( height ), 
            texture( SDL_CreateTextureFromSurface( renderer, atlas ) ) {}
};

struct Object
{
    Sprite sprite;
    SDL_Rect coordinates;
    Object( Sprite sprite, int x, int y ) : sprite( sprite ) {
        coordinates.x = x;
        coordinates.y = y;
        coordinates.w = sprite.width;
        coordinates.h = sprite.height;
    }
    void Move( float x, float y ) {
        coordinates.x += x;
        coordinates.y += y;
    }
    int& x() {
        return coordinates.x;
    }
    int& y() {
        return coordinates.y;
    }
    void Render() {
        SDL_RenderCopy(sprite.renderer, sprite.texture, &sprite.texture_coordinates, &coordinates);
    }
};

struct Vector2D {
	float x, y;
};

template< typename ParameterType >
using CleanType = std::remove_pointer_t< std::remove_reference_t< std::decay_t< ParameterType > > >;

template< typename ScalarParameterType >
concept ScalarConcept = std::is_integral< CleanType< ScalarParameterType > >::value || std::is_floating_point< CleanType< ScalarParameterType > >::value;

template< typename VectorParameterType >
concept Vector2Concept = requires( CleanType< VectorParameterType > vector ) { { vector.x } -> ScalarConcept;
	{ vector.y } -> ScalarConcept;
};

template< typename VectorParameterType >
concept Vector3Concept = requires( CleanType< VectorParameterType > vector ) {
	{ vector } -> Vector2Concept;
	{ vector.z } -> ScalarConcept;
};

template< typename VectorParameterType >
concept VectorConcept = Vector2Concept< VectorParameterType > || Vector3Concept< VectorParameterType >;

template< typename VectorParameterType, typename ComponentParameterType >
requires Vector2Concept< VectorParameterType > || Vector3Concept< VectorParameterType >
struct VectorComponentTypeFinder
{
	const ComponentParameterType& component;
	using ComponentType = CleanType< decltype( component ) >;
	VectorComponentTypeFinder( VectorParameterType vector ) : component( vector.x ) {}
};

template< VectorConcept VectorParameterType >
using CoordinateType = decltype( CleanType< VectorParameterType >{ 0, 0 }.x );//decltype( VectorComponentTypeFinder{ VectorParameterType{ 0, 0 } } )::ComponentType;

template< 
		std::integral auto GameScaleParameterConstant = default_game_scale, 
		Vector2Concept ScreenCoordinatesParameterType = SDL_Point 
	>
Vector2Concept auto to_screen_coordinates( Vector2Concept auto game_coordinates )
{
	using ScreenComponentType = CoordinateType< ScreenCoordinatesParameterType >;
	return ScreenCoordinatesParameterType{ 
			static_cast< ScreenComponentType >( game_coordinates.x / GameScaleParameterConstant ), 
			static_cast< ScreenComponentType >( game_coordinates.y / GameScaleParameterConstant ) 
		};
}

template< 
		std::integral auto GameScaleParameterConstant = default_game_scale, 
		Vector2Concept GameCoordinatesParameterType = SDL_Point 
	>
Vector2Concept auto to_game_coordinates( Vector2Concept auto screen_coordinates )
{
	using GameComponentType = CoordinateType< GameCoordinatesParameterType >;
	return GameCoordinatesParameterType{ 
			static_cast< GameComponentType >( screen_coordinates.x * GameScaleParameterConstant ), 
			static_cast< GameComponentType >( screen_coordinates.y * GameScaleParameterConstant ) 
		};
}

template< ScalarConcept NumericParameterType >
NumericParameterType random_range( NumericParameterType begin, NumericParameterType end )
{
	static std::default_random_engine generator{};
	std::uniform_real_distribution< double > range{ 
			static_cast< double >( begin ), 
			static_cast< double >( end ) 
		};
	return static_cast< NumericParameterType >( Round( range( generator ) ) );
}

template< std::integral NumericParameterType = int >
auto random_range( NumericRange< NumericParameterType > range ) {
	return random_range( range.begin, range.end );
}

template< std::integral NumericParameterType = int >
auto random_range( NumericParameterType range ) {
	return random_range( static_cast< NumericParameterType >( 0 ), range );
}

auto lerp( std::floating_point auto speed, std::integral auto delta_time ) {
	return Round( speed * delta_time );
}

template< std::integral auto GameScaleParamteterConstant = default_game_scale >
SDL_Point uniform_advance( Vector3Concept auto to_advance, std::floating_point auto speed, std::integral auto elapsed_time )
{
	const auto distance = lerp( speed, elapsed_time, GameScaleParamteterConstant );
	return SDL_Point{
			.x = to_advance.x + distance, 
			.y = to_advance.y + distance 
		};
}

template< std::integral auto GameScaleParamteterConstant = default_game_scale >
auto advance( Vector2Concept auto to_advance, Vector2Concept auto velocity, std::integral auto elapsed_time )
{
	return decltype( to_advance ) {
			.x = to_advance.x + lerp( velocity.x, elapsed_time, GameScaleParamteterConstant ), 
			.y = to_advance.y + lerp( velocity.y, elapsed_time, GameScaleParamteterConstant ) 
		};
}

template< 
		bool GameSpaceParameterConstant = false, 
		std::integral auto GameScaleParamteterConstant = default_game_scale 
	>
auto random_point( Vector2Concept auto bounds )
{
	return decltype( bounds ) { 
			random_range( bounds.x ), 
			random_range( bounds.y ) 
		};
}

using TwoPointType = std::array< SDL_Point, 2 >;

TwoPointType sdl_rect_to_points( SDL_Rect rectangle )
{
	return TwoPointType{ 
			SDL_Point{ 
					rectangle.x, 
					rectangle.y
			}, 
			SDL_Point{ 
					rectangle.w, 
					rectangle.h 
				}
		};
}
template< typename PointsParameterType >
SDL_Rect points_to_sdl_rect( PointsParameterType two_points )
{
	return SDL_Rect{ 
			.x = two_points[ 0 ].x, 
			.y = two_points[ 0 ].y,  
			.w = two_points[ 1 ].x, 
			.h = two_points[ 1 ].y 
		};
}

template< std::integral auto GameScaleParameterConstant = default_game_scale >
SDL_Rect sdl_rect_to_screen_coordinates( SDL_Rect rectangle )
{
	auto points_rectangle = sdl_rect_to_points( rectangle );
	for( size_t ii = 0; ii < points_rectangle.size(); ++ii ) {
		points_rectangle[ ii ] = to_screen_coordinates< 
				GameScaleParameterConstant >( points_rectangle[ ii ] );
	}
	return points_to_sdl_rect( points_rectangle );
}


template< 
		auto SDLScreenSpaceCallParameterConstant, 
		std::integral auto GameScaleParameterConstant = default_game_scale 
	>
void sdl_draw_call( SDL_Renderer* renderer, SDL_Point coordinates, auto... parameters ) {
	auto point = to_screen_coordinates< GameScaleParameterConstant >( coordinates );
	SDLScreenSpaceCallParameterConstant( renderer, point.x, point.y, parameters... );
}


template< 
		auto SDLScreenSpaceCallParameterConstant, 
		std::integral auto GameScaleParameterConstant = default_game_scale 
	>
void sdl_draw_call( SDL_Renderer* renderer, SDL_Rect game_rectangle, auto... parameters )
{
	auto screen_space_rectangle = sdl_rect_to_screen_coordinates< 
			SDL_Point, GameScaleParameterConstant >( game_rectangle );
	SDLScreenSpaceCallParameterConstant( renderer, parameters..., &screen_space_rectangle );
}



SDL_Color choose_color( const ColorRangeType color_range )
{
	SDL_Color color = {
			.r = random_range( color_range[ 0 ] ), 
			.g = random_range( color_range[ 1 ] ), 
			.b = random_range( color_range[ 2 ] ), 
			.a= random_range( color_range[ 3 ] ) 
		};
	return color;
}

const ColorRangeType default_star_color_range = { 
			NumericRange< Uint8 >( 250, 255 ), 
			NumericRange< Uint8 >( 200, 250 ), 
			NumericRange< Uint8 >( 250, 255 ), 
			NumericRange< Uint8 >( 254, 255 )
		};

template< std::integral auto GameScaleParamteterConstant = default_game_scale >
void DrawStar( 
		SDL_Renderer* renderer, 
		const SDL_Point& star, 
		const ColorRangeType color_range = default_star_color_range 
	) 
{
	const auto color = choose_color( color_range );
        SDL_SetRenderDrawColor( 
			renderer, 
			color.r, 
			color.g, 
			color.b, 
			color.a
		);
	sdl_draw_call< SDL_RenderDrawPoint >( renderer, star );
}

template< std::integral auto GameScaleParameterConstant = default_game_scale >
std::vector< SDL_Point > InitializeStars( const size_t number_of_stars, const SDL_Point bounds )
{
	std::vector< SDL_Point > stars;
	for( size_t ii = 0; ii < number_of_stars; ++ii )
		stars.push_back( random_point( to_game_coordinates< GameScaleParameterConstant >( bounds ) ) );
	return stars;
}


const size_t default_star_reset_reduction_scalar = 10;

template< std::integral auto GameScaleParameterConstant = default_game_scale >
void DrawStars( 
		SDL_Renderer* renderer, 
		std::vector< SDL_Point >& stars, 
		float star_speed, 
		Uint32 elapsed_time, 
		const SDL_Point bounds, 
		size_t star_reset_reduction_scalar = default_star_reset_reduction_scalar,  
		const ColorRangeType star_color_range = default_star_color_range 
	)
{
	const size_t number_of_stars = stars.size();
	const SDL_Point game_scale_bounds = to_game_coordinates( bounds );
	const int move_distance = lerp( star_speed, elapsed_time * GameScaleParameterConstant );
	for( SDL_Point& star : stars )
	{
		star.y += move_distance;
		DrawStar( renderer, star, star_color_range );
		if( star.y >= game_scale_bounds.y && random_range( number_of_stars / star_reset_reduction_scalar ) == 1 ) {
			star.y = random_range( -game_scale_bounds.y, game_scale_bounds.y );
			star.x = random_range( game_scale_bounds.x );
		}
	}
}


Object MakeShot( SDL_Rect shooter, Sprite shot_sprite, bool top_or_bottom )
{
    return Object {
        shot_sprite, 
        shooter.x + ( shooter.w / 2 ), 
        top_or_bottom == true ? shooter.y : shooter.y - shooter.h
    };
}

int main()
{
    SDL_Point window_bounds = { 
	    	.x = 800, 
		.y = 600 
	};
    SDL_Init( SDL_INIT_VIDEO );
    SDL_Window* window = SDL_CreateWindow(
            "Space Invaders", 
            SDL_WINDOWPOS_CENTERED, 
            SDL_WINDOWPOS_CENTERED, 
	    window_bounds.x, 
	    window_bounds.y, 
            SDL_WINDOW_SHOWN
        );
    SDL_Renderer *renderer = SDL_CreateRenderer( window, -1, 0 );
    bool quit = false;
    SDL_Event e;
    SDL_Surface* test_surface = IMG_Load( "assets/BugSprites.png" );
    auto surface_color_key = SDL_MapRGB( test_surface->format, 0, 0, 0 );
    SDL_SetColorKey( test_surface, SDL_TRUE, surface_color_key );
    SDL_Texture* test_texture = SDL_CreateTextureFromSurface( renderer, test_surface );
    auto stars = InitializeStars( 1000, window_bounds );
    SDL_Rect ship_rect = {
        .x = 58, 
        .y = 168, 
        .w = 86 - 58, 
        .h = 189 - 168
    };
    SDL_Rect playa = {
        .x = 400,
        .y = 500,
        .w = 64,
        .h = 64
    };
    SDL_Rect playa_bullet = {
        .x = 345, 
        .y = 209, 
        .w = 14, 
        .h = 20 
    };
    Sprite bullet_sprite{ renderer, test_surface, playa_bullet, 8, 16 };
    std::vector< Object > playa_shots;
    float playa_speed = .75f;
    int color = 0;
    int last_color = 0;
    float bullet_speed = 2.f;
    Uint64 elapsed_time = 100;
    Uint64 start_ticks = SDL_GetTicks64();
    while( !quit )
    {
        start_ticks = SDL_GetTicks64();
        SDL_SetRenderDrawColor( renderer, 0, 0, 0, 0 );
        SDL_RenderClear( renderer );
		DrawStars( renderer, stars, .01f, elapsed_time, window_bounds );
        SDL_RenderCopy(renderer, test_texture, &ship_rect, &playa);
        for( size_t i = 0; i < playa_shots.size(); ++i )
        {
            playa_shots[ i ].y() -= Round( bullet_speed * elapsed_time );
            playa_shots[ i ].Render();
        }
        SDL_RenderPresent(renderer);
        
        while( SDL_PollEvent( &e ) != 0 )
        {
            switch (e.type)
            {
                case SDL_QUIT : {
                    quit = true;
                    break;
                }
                case SDL_KEYDOWN
                : { 
                    switch (e.key.keysym.sym)
                    {
                        case SDLK_SPACE 
                        : {
                            if( e.key.repeat == 0 )
                                playa_shots.push_back( MakeShot( playa, bullet_sprite, true ) );
                            break;
                        }
                        default : break;
                    }
                    break;
                }
                default : break;
            }
        }
        auto* keyboard_state = SDL_GetKeyboardState( NULL );
        float speed = playa_speed * elapsed_time;
        if( keyboard_state[ SDL_SCANCODE_LEFT ] )
            playa.x -= Round( speed );
        if( keyboard_state[ SDL_SCANCODE_RIGHT ] )
            playa.x += Round( speed );
        elapsed_time = SDL_GetTicks64() - start_ticks;
    }
    return 0;
}

