import subprocess
import pathlib
import os
from dataclasses import dataclass

#TODO: Replace with path
X11_SOCKET = "/tmp/.X11-unix/"

#TODO: Put into Podman and Docker hash-maps
DEFINE_ENVIORNMENT_VARIABLE = "-e"
DEFINE_VOLUME = "-v"
MAP_PORT = "-p"
OPERATOR_MAP_VOLUME = ":"
OPERATOR_ASSIGN_ENVIORNMENT_VARIABLE = "="
OPERATOR_MAP_PORT = ":"

COMMAND_OPTION_NAME = "--name"
COMMAND_OPTION_INTERACTIVE = "-it"
COMMAND_OPTION_REMOVE = "--rm"
COMMAND_OPTION_DEVICE = "--device"
COMMAND_OPTION_GROUP_ADD = "--group-add"
#TODO: Add this in later
#COMMAND_OPTION_OPTION_GROUP_ADD_KEEP_GROUPS = "keep-groups"
COMMAND_RUN = "run"

PODMAN_EXECUTOR = "podman"

#TODO: buildah bud --tag firefox_minimal

def map_volume( 
			system : str, 
			container : str, 
			flag : str = DEFINE_VOLUME, 
			operator : str = OPERATOR_MAP_VOLUME 
		) -> str: 
	return ( flag, ''.join( [ system, operator, container ] ) )

def map_port( 
		host : str, 
		container : str, 
		flag : str = MAP_PORT, 
		operator : str = OPERATOR_MAP_PORT 
		): 
	return ( flag, ''.join( [ host, operator, container ] ) )

def define_enviornment_variable( 
			container : str, 
			system : str, 
		) -> str:
	return map_volume( 
			container, 
			system, 
			DEFINE_ENVIORNMENT_VARIABLE, 
			OPERATOR_ASSIGN_ENVIORNMENT_VARIABLE 
		)


########################################################
# Carry platform information, derived classes ##########
# carry platform-dependant information per device-type #
########################################################

@dataclass
class DeviceOptions: 
	DISPLAY_OS_X11_LINUX = "X11Linux"
	PULSE_AUDIO_LINUX = "PulseAudioLinux"
	host_os : str = DISPLAY_OS_X11_LINUX
	container_os : str = DISPLAY_OS_X11_LINUX

@dataclass
class X11LinuxToX11Linux( DeviceOptions ): 
	DISPLAY_ENVIORNMENT_VARIABLE = "DISPLAY"
	DISPLAY_DEFAULT_VALUE = os.environ[ "DISPLAY" ]
	host_os : str = DeviceOptions.DISPLAY_OS_X11_LINUX
	container_os : str = DeviceOptions.DISPLAY_OS_X11_LINUX
	display_container : str = DISPLAY_ENVIORNMENT_VARIABLE
	display_system : str = DISPLAY_DEFAULT_VALUE
	display_socket_system : str = X11_SOCKET
	display_socket_container : str = X11_SOCKET

def add_display_x11_linux_x11_linux( options : X11LinuxToX11Linux = X11LinuxToX11Linux() ) -> list[str]: 
	return [ 
			*define_enviornment_variable( 
					options.display_container, 
					options.display_system 
				), 
			*map_volume( 
					options.display_socket_system, 
					options.display_socket_container 
				) 
		]

def add_display( options : DeviceOptions = None ) -> list[str]: 
	#x11_linux to x11_linux is currently the only system supported
	if not options: 
		return add_display_x11_linux_x11_linux()
	return add_display_x11_linux_x11_linux( options )

class DefaultLinuxWebcamToDefaultLinuxWebcam( DeviceOptions ): 
	device : str = "/dev/video0"

def add_default_linux_webcam_default_linux_webcam( options : DefaultLinuxWebcamToDefaultLinuxWebcam 
			= DefaultLinuxWebcamToDefaultLinuxWebcam() ) -> list[str]: 
	return [ 
			COMMAND_OPTION_DEVICE, 
			options.device 
		]

def add_webcam( options : DeviceOptions = None ) -> list[str]: 
	#default_linux_webcam to default_linux_webcam is currently the only system supported
	if not options: 
		return add_default_linux_webcam_default_linux_webcam()
	return add_default_linux_webcam_default_linux_webcam( options )


class DefaultLinuxAMDGPUToDefaultAMDGPU( DeviceOptions ): 
	device : str = "/dev/dri"
	host_port : str = "14500"
	container_port : str = "14500"

def add_default_linux_amd_gpu_default_linux_amd_gpu( options : DefaultLinuxWebcamToDefaultLinuxWebcam 
			= DefaultLinuxAMDGPUToDefaultAMDGPU() ) -> list[str]: 
	return [ 
			COMMAND_OPTION_DEVICE, 
			options.device, 
		]

def add_amd_gpu( options : DeviceOptions = None ) -> list[str]: 
	#default_linux_webcam to default_linux_amd_gpu is currently the only system supported
	if not options: 
		return add_default_linux_amd_gpu_default_linux_amd_gpu()
	return add_default_linux_amd_gpu_default_linux_amd_gpu( options )

class PulseAudioLinuxToPulseAudioLinux( DeviceOptions ): 
	host_os : str = DeviceOptions.PULSE_AUDIO_LINUX
	container_os : str = DeviceOptions.PULSE_AUDIO_LINUX
	XGD_RUNTIME_DIRECTORY : str = os.environ[ "XDG_RUNTIME_DIR" ]
	device : str = "/dev/snd"
	system_audio_server : str = "PULSE_SERVER"
	container_audio_server : str = "unix:" + XGD_RUNTIME_DIRECTORY + "/pulse/native"
	audio_socket_system : str = XGD_RUNTIME_DIRECTORY + "/pulse/native"
	audio_socket_container : str = XGD_RUNTIME_DIRECTORY + "/pulse/native"
	cookie_system_relative = ".config/pulse/cookie" #~/
	# May need to be configurable later
	cookie_container_absolute = "/root/.config/pulse/cookie" #/root/ 
	groups_to_add : tuple = tuple( [ "audio" ] ) #"--group-add", "audio", 

def add_audio_pulse_audio_linux_pulse_audio_linux( options : PulseAudioLinuxToPulseAudioLinux 
			= PulseAudioLinuxToPulseAudioLinux() ) -> list[str]: 
	groups = []
	for group in options.groups_to_add: 
		groups += [ COMMAND_OPTION_GROUP_ADD, group ]
	return [ 
			COMMAND_OPTION_DEVICE, 
			options.device, 
			*tuple( groups ), 
			*define_enviornment_variable( 
					options.system_audio_server, 
					options.container_audio_server 
				), 
			*map_volume( 
					options.audio_socket_system, 
					options.audio_socket_container 
				), 
			*map_volume( 
					str( pathlib.Path.home() / \
							options.cookie_system_relative ), 
					options.cookie_container_absolute 
				)
		]
	return command_options

def add_audio( options : DeviceOptions = None ) -> list[str]: 
	#pulse_audio_linux to pulse_audio_linux is currently the only system supported
	if not options: 
		return add_audio_pulse_audio_linux_pulse_audio_linux()
	return add_audio_pulse_audio_linux_pulse_audio_linux( options )

def make_run_container_command(
			image_name : str, 
			container_name : str, 
			executor : str, 
			display_options : DeviceOptions = X11LinuxToX11Linux(), 
			audio_options : DeviceOptions = PulseAudioLinuxToPulseAudioLinux(), 
			webcam_options : DeviceOptions = DefaultLinuxWebcamToDefaultLinuxWebcam(), 
			amd_gpu_options : DeviceOptions = DefaultLinuxAMDGPUToDefaultAMDGPU(), 
			use_home : bool = True, 
			optional_volumes : list = (), 
			optional_ports = (), 
			optional_enviornmental_variables = (), 
			optional_arguments = (), 
		) -> str: 
	original_optional_volumes = optional_volumes
	optional_volumes = []
	for file_pair in original_optional_volumes: 
		optional_volumes += list( map_volume( 
				str( file_pair[ 0 ] ), 
				str( file_pair[ 1 ] ) 
			) )
	#print( optional_volumes )
	optional_volumes = tuple( optional_volumes )
	#TODO: Turn into dict?
	return [ 
			executor, 
			COMMAND_RUN, 
			COMMAND_OPTION_INTERACTIVE, 
			COMMAND_OPTION_REMOVE, 
			COMMAND_OPTION_NAME, 
			*container_name, 
			*optional_volumes, 
			*optional_ports, 
			#"--env", "NVIDIA_VISIBLE_DEVICES=all", 
			#"--env", "NVIDIA_DRIVER_CAPABILITIES=display", 
			*optional_enviornmental_variables, 
			*optional_arguments, 
			*tuple( add_display( display_options ) ) 
		] + list( [
			*tuple( add_audio( audio_options ) ) 
		] if audio_options else [] ) + list( [
			*tuple( add_webcam( webcam_options ) ) 
		] if webcam_options else [] ) + list( [ 
			*tuple( add_amd_gpu( amd_gpu_options ) ) 
		] if amd_gpu_options else [] ) + [
			*image_name 
		]

def run_container( 
		command : list[str], 
		image_name : str, 
		container_name : str 
	): 
	print( "Executing with\nImage: ", 
			image_name, 
			"\nConatainer: ", 
			container_name, 
			"\nCommand: " 
		)
	print( command )
	return subprocess.run( command )

